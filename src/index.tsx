import React from "react";
import ReactDOM from "react-dom";
import "./tailwind.output.css"
import "./index.css";
import App from "./App";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from "@apollo/client";
import PeopleProvider from "./context/PeopleProvider";

const client = new ApolloClient({
  uri: "http://localhost:9000/graphql",
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <React.StrictMode>
      <PeopleProvider client={client}>
        <App />
      </PeopleProvider>
    </React.StrictMode>
  </ApolloProvider>,
  document.getElementById("root")
);
