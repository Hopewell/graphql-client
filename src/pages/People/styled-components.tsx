import { styled } from "goober";

export const PeopleContainer = styled("section")`
  max-width: 1024px;
  margin: 0px auto;
  padding: 8px;
`;

export const PeopleHeader = styled("h1")`
  color: #FFE300;
  font-size: 30px;
`;

export const InputContainer = styled("section")`
  padding: 8px;
  display: flex;
  width: 100%;
  height: auto;
  justify-content: center;
`;

export const SearchInput = styled(`input`)`
  width: 100%;
  padding: 16px;
  font-size: 18px;
  border-radius: 4px;
  color: #000;
  border: 4px solid rgba(0, 0, 0, 0);
  outline: none;
  opacity: 0.7;
  position: sticky;
  transition: 0.5s all;

  &:focus {
    border: 4px solid #fdfdfd;
    opacity: 1;
  }
`;

export const Content = styled("section")`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr auto;
  grid-column-gap: 0px;
  grid-row-gap: 16px;
`;

export const PaginationContainer = styled("section")`
  display: flex;
  justify-content: center;
`;

export const PaginationButton = styled<{ isActive: boolean; onClick: any }>(
  "button"
)`
  width: auto;
  padding: 8px;
  margin: 4px;
  background: ${(props) => (props.isActive ? "#000" : "#FFE300")};
  color: ${(props) => (props.isActive ? "#fff" : "#000")};
  border-radius: 4px;
  cursor: pointer;
  outline: none;
  border: none;
`;

export const ListItems = styled("ul")`
  list-style-type: none;
  color: #fff;
  padding-left: 0px;
  display: flex;
  flex-wrap: wrap;

  @media (max-width: 280px) {
    justify-content: center;
  }
`;

export const ListItem = styled("li")`
  width: 100%;
  max-width: 319px;
  border: 2px solid #FFE300;  margin: 8px;
  padding: 16px;
  max-width: 319px;
  border: 1px solid rgba(0,0,0,.125);
  margin: 8px;
  padding: 16px;
  box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
  transition: 0.3s;
  border-radius: 5px;
  background: #263242;
  @media (max-width: 319px) {
    width: 100%;
  }
`;

export const ListTitle = styled("h2")`
  color: #FFE300;
`;

export const ListDescription = styled<{ inverse: boolean }>("p")`
  ${(props) =>
    props.inverse
      ? `
    color: #FFE300;
    font-size: 18px;
  `
      : `color: #fff;`}
`;
